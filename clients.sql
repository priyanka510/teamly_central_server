CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `company_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `rest_api` text COLLATE utf8_unicode_ci NOT NULL,
  `secret_key` text COLLATE utf8_unicode_ci NOT NULL,
  `max_user_count` int(11) NOT NULL DEFAULT 1,
  `subscription` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `appversion` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);


