<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\File;
use App\Exception;
use \Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;

class CompanyController extends Controller
{
    public function get_companies(Request $request)
    {
        try
        {
            Log::info('inside get_companies function');
            $companies=DB::table('clients')->get();
            return response()->json(array('status'=>'sucess','companies'=>$companies));
        }
        catch(Exception $e)
        {
            Log::error($e->getMessage());
            return response()->json(array('status'=>'error','msg'=>'Something went wrong. Try again later.'));
        }
    }

    public function check_subscription(Request $request)
    {
        try
        {
            Log::info('inside check_subscription function');
            $username=$request->username;
            $company = DB::table('users')->join('clients','clients.id','users.company_id')->select('clients.*')->where('users.email',$username)->orwhere('users.mobile',$username)->get();
            
            if(sizeof($company))
            {
                $subscription=$company[0]->subscription;
                $currenttime = date('Y-m-d');
                if($subscription==$currenttime)
                {
                    return response()->json(array(
                        'status'=>'warning',
                        'msg'=>'Please renew your subscription.',
                        'company_name'=>$company[0]->company_name,
                        'company_id'=>$company[0]->id,
                        'rest_api'=>$company[0]->rest_api,
                        'secret_key'=>$company[0]->secret_key,
                        'subscription'=>$company[0]->subscription,
                        'appversion'=>$company[0]->appversion
                    ));
                }
                if($subscription < $currenttime)
                {
                    return response()->json(array('status'=>'Subscription Expired','msg'=>'Please renew your subscription to login'));
                }
                else
                {
                    
                    return response()->json(array(
                        'status'=>'success',
                        'msg'=>'Your subscription is active',
                        'company_name'=>$company[0]->company_name,
                        'company_id'=>$company[0]->id,
                        'rest_api'=>$company[0]->rest_api,
                        'secret_key'=>$company[0]->secret_key,
                        'subscription'=>$company[0]->subscription,
                        'appversion'=>$company[0]->appversion
                    ));
                }
            }
            else
            {
                return response()->json(array('status'=>'Error','msg'=>'User not found.'));
            }
                
        }
        catch(Exception $e)
        {
            Log::error($e->getMessage());
            return response()->json(array('status'=>'error','msg'=>'Something went wrong. Try again later.'));
        }            
    }
    
    public function get_max_user_count(Request $request)
    {
        try
        {
            Log::info('inside get_max_user_count function');
            Log::info($request);
            $max_user_count=DB::table('clients')->where('id',$request->company_id)->value('max_user_count');

            return response()->json(array('status'=>'success','max_user_count'=>$max_user_count));

        }
        catch(Exception $e)
        {
            Log::error($e->getMessage());
            return response()->json(array('status'=>'error','msg'=>'Central Server Facing Issue. Please Raise to Teamly Support Team'));
        }
    }
    
    public function add_user(Request $request)
    {
        try
        {
            $company_id=DB::table('clients')->where('rest_api',$request->rest_call_path)->get(['id' , 'max_user_count', 'version'])[0];
            if($company_id->version=='base' && $request->request_from=='add_emp')
            {
                return response()->json(array('status'=>'success','msg'=>''));
            }
            $count_users=DB::table("users")->where("company_id" ,$company_id->id)->count();
            if($count_users < $company_id->max_user_count)
            {
                $insert=DB::table("users")->insert(["email"=>$request->email , "mobile"=>$request->mobile ,"company_id"=>$company_id->id , "created_at"=>date("Y-m-d H:i:s")]);
                return response()->json(array('status'=>'success','msg'=>'User added successfully'));
            }
            else
            {
                return response()->json(array('status'=>'error','msg'=>'User limit exceeded '));
            }
           
        }
        catch(Exception $e)
        {
            Log::error($e->getMessage());
            return response()->json(array('status'=>'error','msg'=>'Something went wrong while adding user. Try again later.'));
        }
    }
    
    public function update_user(Request $request)
    {
        try
        {
            Log::info('inside update_user function');
            $company_id=DB::table('clients')->where('rest_api',$request->rest_call_path)->get(['id' , 'max_user_count'])[0];
            // $count_users=DB::table("users")->where("company_id" ,$company_id->id)->count();
            // if($count_users < $company_id->max_user_count)
            // {
            $update=DB::table("users")->where('email',$request->old_email)->where('mobile',$request->old_mobile)->where('company_id',$company_id->id)->update(["email"=>$request->email , "mobile"=>$request->mobile]);
            return response()->json(array('status'=>'success','msg'=>'User updated successfully'));
            // }
            // else
            // {
            //     return response()->json(array('status'=>'error','msg'=>'User cannot be updated'));
            // }
           
        }
        catch(Exception $e)
        {
            Log::error($e->getMessage());
            return response()->json(array('status'=>'error','msg'=>'Something went wrong while updating user. Try again later.'));
        }
    }
    
    public function delete_user(Request $request)
    {
        try
        {
            Log::info('Delete User function');
            $company_id=DB::table('clients')->where('rest_api',$request->rest_call_path)->value('id');
            $delete=DB::table("users")->where("email" ,$request->email)->where("mobile" ,$request->mobile)->where('company_id',$company_id)->delete();
            if($delete)
            {
                return response()->json(array('status'=>'success','msg'=>'User removed successfully'));
            }
            else
            {
                return response()->json(array('status'=>'error','msg'=>'User not  removed '));
            }
           
        }
        catch(Exception $e)
        {
            Log::error($e->getMessage());
            return response()->json(array('status'=>'error','msg'=>'Something went wrong when adding user . Try again later.'));
        }
    }
    
    public function maxUserLimit(Request $request)
    {
        try
        {
            Log::info('Max user limit check function ');
            $company_id=DB::table('clients')->where('rest_api',$request->rest_call_path)->value('id');
            $active_user=DB::table("users")->where("company_id", $company_id)->count();
            if(!empty($company_id))
            {  
                $maxUser=DB::table('clients')->where('rest_api',$request->rest_call_path)->value('max_user_count');
                Log::info($maxUser."  Central server ".$active_user);
                return response()->json(array('status'=>'success','msg'=>'User added successfully' , "maxUser"=>$maxUser , "activeUser"=>$active_user));
            }
            else
            {
                return response()->json(array('status'=>'error','msg'=>'Something went wrong when adding user . Try again later.'));
            }
        }
        catch(Exception $e)
        {
            Log::error($e->getMessage());
            return response()->json(array('status'=>'error','msg'=>'Something went wrong when adding user . Try again later.'));
        }
    }
    
    public function checkForUser(Request $request)
    {
        try
        {
            if(filter_var($request->username, FILTER_VALIDATE_EMAIL) || preg_match('/^[0-9]{10}+$/', $request->username))
            {
                $url='';
                $user_company_id= DB::table("users")->where("email" , $request->username)->orwhere("mobile" , $request->username)->value("company_id");
                if(!empty($user_company_id))
                {
                    $url=DB::table("clients")->where("id" , $user_company_id)->value("rest_api");
                    return response()->json(array('status'=>'success','msg'=>'User is present' , "url"=>$url ));
                }
                else
                {
                    return response()->json(array('status'=>'error','msg'=>'User is not  present' ));
                }
            }
            else
            {
                return response()->json(array('status'=>'error','msg'=>'Invalid Username'));
            }
            
        }
        catch(Exception $e)
        {
            Log::error($e->getMessage());
            return response()->json(array('status'=>'error','msg'=>'Something went wrong when adding user . Try again later.'));
        }
        
    }
    
}
