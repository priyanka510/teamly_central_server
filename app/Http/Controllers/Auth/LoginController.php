<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\File;
use App\Exception;
use \Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        try
        {
            Log::info('inside login function');

            if (Auth::attempt(['email' => $request->email, 'password' => $request->password]))
            {
                $user = Auth::user();
                $company=DB::table('clients')->where('id',$user->company_id)->first();
                $rest_api_path=$company->rest_api;
                $appversion=$company->appversion;
                if((int)str_replace('.', "", $appversion)>(int)str_replace('.', "", $request->appversion))
                {
                    return response()->json(array(
                        'status'=>'error',
                        'msg'=>'You are using lower version. Please <a href="https://play.google.com/store/apps/details?id=app.softomatic.teamlyhrm">update</a> your app'
                    ));
                }
                else
                return response()->json(array(
                    'status'=>'success',
                    'msg'=>'Login successfully',
                    'rest_api'=>$rest_api_path,
                    'comapany_id'=>$user->company_id,
                    'user_id'=>$user->id
                    'username'=>$user->username
                    'email'=>$user->email
                ));
            }
            else
            {
                return response()->json(array(['status'=>'error','msg'=>'Invalid username or password']));
            }
        }
        catch(Exception $e)
        {
            Log::error($e->getMessage());
            return response()->json(array(['status'=>'error','msg'=>'Something went wrong. Try again later.']));
        }
    }

    public function get_latest_version(Request $request)
    {
        try
        {
            Log::info('inside get_latest_version function');
            $appversion=DB::table('clients')->where('id',$request->company_id)->value('appversion');
            return response()->json(array('status'=>'success','latest_version'=>$appversion));
        }
        catch(Exception $e)
        {
            Log::error($e->getMessage());
            return response()->json(array(['status'=>'error','msg'=>'Something went wrong. Try again later.']));
        }
    }

    public function update_registration_id(Request $request)
    {
        try
        {
            Log::info('inside update_registration_id function');
            $user_id=DB::table('users')->where('email',$request->email)->value('id');
            $update=-1;
            if(DB::table('users_firebase_id')->where('user_id',$user_id)->count()>0)
            {
                $update=DB::table('users_firebase_id')->where('user_id',$user_id)->update(['firebase_id'=>$request->registration_id]);
            }
            else
                $update=DB::table('users_firebase_id')->insert(['user_id'=>$user_id,'firebase_id'=>$request->registration_id]);

            if($update!=-1)
                return response()->json(array('status'=>'success','msg'=>'Firebase registration ID updated successfully'));
            else
                return response()->json(array('status'=>'error','msg'=>'Firebase registration ID could not be updated'));
        }
        catch(Exception $e)
        {
            Log::error($e->getMessage());
            return response()->json(array(['status'=>'error','msg'=>'Something went wrong. Try again later.']));
        }
    }
}
