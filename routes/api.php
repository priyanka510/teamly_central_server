<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('login', 'Auth\LoginController@login')->middleware('checkKey','cors'); 
Route::get('get_companies', 'CompanyController@get_companies')->middleware('checkKey','cors'); 
Route::post('check_subscription', 'CompanyController@check_subscription')->middleware('checkKey','cors');
Route::post('get_max_user_count', 'CompanyController@get_max_user_count')->middleware('checkKey','cors'); 
Route::post('add_user', 'CompanyController@add_user')->middleware('checkKey','cors');
Route::post('maxUserLimit', 'CompanyController@maxUserLimit')->middleware('checkKey','cors');
Route::post('delete_user','CompanyController@delete_user')->middleware('checkKey','cors');
Route::post('update_user','CompanyController@update_user')->middleware('checkKey','cors');
Route::post('checkForUser','CompanyController@checkForUser')->middleware('cors');
